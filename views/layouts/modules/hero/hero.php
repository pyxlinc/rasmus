<?php
$hero = $layouts[0];
?>
<section class="hero">
    <h2><?php echo esc_html($hero['title']); ?></h2>
    <?php echo esc_html($hero['description']); ?>
    <?php if ($hero['link']) : ?>
        <a href="<?php echo esc_url($hero['link']['url']); ?>">
            <span><?php echo esc_html($hero['link']['title']) ?: 'Read More'; ?></span>
        </a>
    <?php endif; ?>
</section>