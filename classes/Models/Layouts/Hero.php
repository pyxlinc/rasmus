<?php

namespace Pyxl\RENAME\Models\Layouts;

use Pyxl\Layouts\Model;

class Hero extends Model
{
    public function __construct()
    {
        $this->sub_fields = [
            [
                'label' => 'Title',
                'slug'  => 'title',
                'type'  => 'text',
            ],
            [
                'label' => 'Description',
                'slug'  => 'description',
                'type'  => 'textarea',
            ],
            [
                'label' => 'Link',
                'slug'  => 'link',
                'type'  => 'link',
            ],
        ];

        parent::__construct($this);
    }
}
